/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashing;

/**
 *
 * @author User
 */
public class Hashing {

    /**
     * @param args the command line arguments
     */
    public static boolean isSubset(int arr1[], int arr2[], int x, int y) {
        int i = 0;
        int j = 0;
        for (i = 0; i < y; i++) {
            for (j = 0; j < x; j++) {
                if (arr2[i] == arr1[j]) {
                    break;
                }
            }
            if (j == x) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int arr1[] = {5, 20, 16, 31, 19};
        int arr2[] = {5, 16, 20};
        int x = arr1.length;
        int y = arr2.length;
        if (isSubset(arr1, arr2, x, y)) {
            System.out.println("Array2[] is Subset of Array1[]");
        } else {
            System.out.println("Array2[] is not Subset of Array1[]");
        }
    }

}
